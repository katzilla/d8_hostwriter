<?php
/**
 * @file
 * Contains \Drupal\hostwriter_migration\Plugin\migrate\source\Users.
 */

namespace Drupal\hostwriter_migration\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;
use function strlen;


/**
 * Extract users from Drupal 7 database.
 *
 * @MigrateSource(
 *    id = "hostwriter_users",
 *    source_module = "hostwriter_migration"
 * )
 */
class Users extends FieldableEntity {

  /**
   * {@inheritdoc}
   */
  public function query() {
    return $this->select('users', 'u')
      ->fields('u')
      ->condition('u.uid', 1, '>');
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'uid' => $this->t('User ID'),
      'name' => $this->t('Username'),
      'field_first_name' => $this->t('First Name'),
      'field_media' => $this->t('I work in'),
      'field_expertise' => $this->t('My expertise'),
      'field_organisations' => $this->t('I\'m a member of'),
      'field_last_name' => $this->t('Last Name'),
      'field_offer' => $this->t('As a hostwriter I can offer'),
      'field_contact_team' => $this->t('Yes, I would like to receive occasional updates with collaboration or grant opportunities specific to my journalistic profile.'),
      'field_twitter' => $this->t('Twitter Account'),
      'field_twitter_mentions' => $this->t('I agree that hostwriter mentions my membership on Twitter.'),
      'field_location' => $this->t('I am based in'),
      'field_link_item' => $this->t('Work samples'),
      'pass' => $this->t('Password'),
      'mail' => $this->t('Email address'),
      'signature' => $this->t('Signature'),
      'signature_format' => $this->t('Signature format'),
      'created' => $this->t('Registered timestamp'),
      'access' => $this->t('Last access timestamp'),
      'login' => $this->t('Last login timestamp'),
      'status' => $this->t('Status'),
      'timezone' => $this->t('Timezone'),
      'language' => $this->t('Language'),
      'picture' => $this->t('Picture'),
      'init' => $this->t('Init'),
      'data' => $this->t('User data'),
      'roles' => $this->t('Roles'),
      'full_name' => $this->t('Custom'),
    ];

    // Profile fields.
    if ($this->moduleExists('profile')) {
      $fields += $this->select('profile_fields', 'pf')
        ->fields('pf', ['name', 'title'])
        ->execute()
        ->fetchAllKeyed();
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    include 'expertise_map.php';
    
    $uid = $row->getSourceProperty('uid');

    $roles = $this->select('users_roles', 'ur')
      ->fields('ur', ['rid'])
      ->condition('ur.uid', $uid)
      ->execute()
      ->fetchCol();

    $row->setSourceProperty('roles', $roles);

     $firstname = $this->select('field_data_field_first_name', 'fn')
      ->fields('fn', ['field_first_name_value'])
      ->condition('fn.entity_id', $uid)
      ->execute()
      ->fetchCol();

    $lastname = $this->select('field_data_field_last_name', 'ln')
      ->fields('ln', ['field_last_name_value'])
      ->condition('ln.entity_id', $uid)
      ->execute()
      ->fetchCol();

    $row->setSourceProperty('full_name', $firstname[0] . ' ' . $lastname[0]);

    $row->setSourceProperty('data', unserialize($row->getSourceProperty('data')));

    // If this entity was translated using Entity Translation, we need to get
    // its source language to get the field values in the right language.
    // The translations will be migrated by the d7_user_entity_translation
    // migration.
    $entity_translatable = $this->isEntityTranslatable('user');
    $source_language = $this->getEntityTranslationSourceLanguage('user', $uid);
    $language = $entity_translatable && $source_language ? $source_language : $row->getSourceProperty('language');
    $row->setSourceProperty('entity_language', $language);

    // Get Field API field values.
    foreach ($this->getFields('user') as $field_name => $field) {
      // Ensure we're using the right language if the entity and the field are
      // translatable.
      $field_language = $entity_translatable && $field['translatable'] ? $language : NULL;
      $row->setSourceProperty($field_name, $this->getFieldValues('user', $field_name, $uid, NULL, $field_language));
    }

    // Get profile field values. This code is lifted directly from the D6
    // ProfileFieldValues plugin.
    if ($this->getDatabase()->schema()->tableExists('profile_value')) {
      $query = $this->select('profile_value', 'pv')
        ->fields('pv', ['fid', 'value']);
      $query->leftJoin('profile_field', 'pf', 'pf.fid=pv.fid');
      $query->fields('pf', ['name', 'type']);
      $query->condition('uid', $row->getSourceProperty('uid'));
      $results = $query->execute();

      foreach ($results as $profile_value) {
        if ($profile_value['type'] == 'date') {
          $date = unserialize($profile_value['value']);
          $date = date('Y-m-d', mktime(0, 0, 0, $date['month'], $date['day'], $date['year']));
          $row->setSourceProperty($profile_value['name'], ['value' => $date]);
        }
        elseif ($profile_value['type'] == 'list') {
          // Explode by newline and comma.
          $row->setSourceProperty($profile_value['name'], preg_split("/[\r\n,]+/", $profile_value['value']));
        }
        else {
          $row->setSourceProperty($profile_value['name'], [$profile_value['value']]);
        }
      }
    }


    /*
     * Get profile2 fields from d7-database: "About me"-profile fields
     * 
     */
    $query_profile_main = $this->select('profile', 'p');
    $query_profile_main->fields('p', array('pid', 'uid', 'type'));
    $query_profile_main->condition('p.uid', $uid, '=');
    $query_profile_main->condition('p.type', 'main', '=');
    
    /*
     * Get Twitter field and mentions from main profile:
     */
    $query_profile_main->leftjoin('field_data_field_twitter', 'twitter', 'twitter.entity_id = p.pid');
    $query_profile_main->leftjoin('field_data_field_twitter_mentions', 'twitter_mentions', 'twitter_mentions.entity_id = p.pid');
    $query_profile_main->addField('twitter', 'field_twitter_value');
    $query_profile_main->addField('twitter_mentions', 'field_twitter_mentions_value');
    $result = $query_profile_main->execute();
    foreach ($result as $record) {
      $row->setSourceProperty('field_twitter', $record['field_twitter_value'] );
      $row->setSourceProperty('field_twitter_mentions', $record['field_twitter_mentions_value'] );
    }

    /*
     * Get Bio field from main profile:
     */
    $query_profile_main->leftjoin('field_data_field_bio', 'bio', 'bio.entity_id = p.pid');
    $query_profile_main->addField('bio', 'field_bio_value');
    $result = $query_profile_main->execute();
    foreach ($result as $record) {
      $row->setSourceProperty('field_bio', $record['field_bio_value'] );
    }

    /*
     * Get Address field from main profile:
     */
    $query_profile_main->leftjoin('field_data_field_location', 'location', 'location.entity_id = p.pid');
    $query_profile_main->addField('location', 'field_location_country');
    $query_profile_main->addField('location', 'field_location_administrative_area');
    $query_profile_main->addField('location', 'field_location_locality');
    $result = $query_profile_main->execute();
    foreach ($result as $record) {
      $address = [
        'country_code' => $record['field_location_country'],
        'locality' => $record['field_location_locality'],
        'administrative_area' => $record['field_location_administrative_area'],
      ];
      $row->setSourceProperty('field_location', $address);
    }

    /*
     * Get Languages fieldgroup data from main profile:
     */
    $query_profile_main->leftjoin('field_data_field_languages', 'languages', 'languages.entity_id = p.pid');
    $query_profile_main->leftjoin('field_data_field_language', 'language', 'languages.field_languages_value = language.entity_id');
    $query_profile_main->leftjoin('field_data_field_language_level', 'level', 'languages.field_languages_value = level.entity_id');
    $query_profile_main->addField('language', 'field_language_value');
    $query_profile_main->addField('level', 'field_language_level_value');
    $result = $query_profile_main->execute();

    $delta = 0;
    foreach ($result as $record) {
      $level = 0;
      switch ($record['field_language_level_value']) {
        case "beginner":
          $level = 2;
          break;
        case "intermediate":
          $level = 1;
          break;
        case "fluent":
          $level = 0;
          break;
      }
      $languages[$delta] = [
        'language' => $record['field_language_value'],
        'level' => $level
      ];

      $row->setSourceProperty('field_languages', $languages);
      $delta++;
    }

   /*
   * Get profile2 fields from d7-database: "About my work"-profile fields
   * map old tid with new ones
   */
    $expertise = [];
    $organisations = [];
    $media = [];
    $expertiseMap = include("expertise_map.php");
    $organisationsMap = include("organisations_map.php");
    $mediaMap = array(
      12321 => 4, # Print
      7588 => 5, # Online
      6919 => 6, # Radio
      6920 => 7, # Television
      12322 => 8, # Multimedia
      6912 => 9, # Photo Journalism
      12323 => 10 # Documentary film
    );
    $query_profile_work = $this->select('profile', 'p');
    $query_profile_work->fields('p', array('pid', 'uid', 'type'));
    $query_profile_work->condition('p.uid', $uid, '=');
    $query_profile_work->condition('p.type', 'work', '=');
    /*
     * Get media terms from work profile:
     */
    $query_profile_work->leftjoin('field_data_field_media', 'media', 'media.entity_id = p.pid');
    $query_profile_work->addField('media', 'field_media_tid');
    $query_profile_work->addField('media', 'delta');
    $result_media = $query_profile_work->execute();
    foreach ($result_media as $record) {
      $media[] = [
        'delta' => $record['delta'],
        'target_id' => $mediaMap[$record['field_media_tid']],
        'original' => $record['field_media_tid']
      ];
      $row->setSourceProperty('field_media', $media);
    }
    
    $query_profile_work->leftjoin('field_data_field_expertise', 'expertise', 'expertise.entity_id = p.pid');
    $query_profile_work->addField('expertise', 'field_expertise_tid');
    $query_profile_work->addField('expertise', 'delta');
    $result_expertise = $query_profile_work->execute();
    foreach ($result_expertise as $record) {
      // only map if possible
      if ($expertiseMap[$record['field_expertise_tid']]) {
        $expertise[] = [
          'delta' => $record['delta'],
          'target_id' => $expertiseMap[$record['field_expertise_tid']],
          'original' => $record['field_expertise_tid']
        ];
        $row->setSourceProperty('field_expertise', $expertise);
      }
    }
    
    $query_profile_work->leftjoin('field_data_field_organisations', 'organisations', 'organisations.entity_id = p.pid');
    $query_profile_work->addField('organisations', 'field_organisations_tid');
    $query_profile_work->addField('organisations', 'delta');
    $result_organisations = $query_profile_work->execute();
    foreach ($result_organisations as $record) {
      if($organisationsMap[$record['field_organisations_tid']]) {
        $organisations[] = [
          'delta' => $record['delta'],
          'target_id' => $organisationsMap[$record['field_organisations_tid']],
        ];
        $row->setSourceProperty('field_organisations', $organisations);
      }
    }

    $query_profile_work->leftjoin('field_data_field_link_item', 'worksamples', 'worksamples.entity_id = p.pid');
    $query_profile_work->addField('worksamples', 'field_link_item_url');
    $query_profile_work->addField('worksamples', 'field_link_item_title');
    $result_worksamples = $query_profile_work->execute();
    foreach ($result_worksamples as $record) {
      $title = $record['field_link_item_title'] ? substr($record['field_link_item_title'],0,140)."..." : substr($record['field_link_item_url'],0,140)."..." ;
      $worksamples[] = [
        'uri' => $record['field_link_item_url'],
        'title' => $title,
      ];
      $row->setSourceProperty('field_link_item', $worksamples);
    }

    return parent::prepareRow($row);
  }


  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'uid' => [
        'type' => 'integer',
        'alias' => 'u',
      ],
    ];
  }

}

?>
