<?php

return array(
  /* Activism/Advocacy (32) */
  5543 => 32, # 5543: activism 
  6878 => 32, # 6878: Social Activism  
  7984 => 32, # 7984: local activism 
  10420 => 32, # 10420: encounters and activism 
  8460 => 32, # 8460: advocacy
  /* Agriculture (33) */
  4553 => 33, # 4553: agriculture  
  8121 => 33, # 8121: agriculture...
  9769 => 33, # 9769: agriculture and Business
  /* Animal rights */
  8119 => 34, #8119 : animals
  13101 => 34, #13101 : Animal Rights
  13102 => 34, #13101 : Animal Abuse
  12553 => 34, #12553 : Animal Welfare
  /* Architecture (35) */
  4685 => 35, # 4685: architecture 
  /* Arms/Weapons trade (36) */
  4428 => 36, #4428: arms
  5067 => 36, # 5067: arms deals  6261: arms trade
  6261 => 36, # 6261: arms trade
  /* Art & Culture (37) */
  12402 => 37, #12402: Art & Culture 
  12165 => 37, #12165: Arts & Culture
  8376 => 37, # 8376: Arts + Culture 
  4157 => 37, #4157: arts and culture
  3952 => 37, # 3952: culture 
  3895 => 37, #3895: youth culture 
  5567 => 37, #5567: work culture
  5182 => 37, # 5182: urban culture 
  11219 => 37, #11219: culture etc 
  9046 => 37, #9046: culture... 
  6145 => 37, #6145: internet culture 
  10321 => 37, #10321: japanese culture 
  4234 => 37, #4234: jewish culture
  10120 => 37, #10120: language, culture; social issues
  10139 => 37, #10139: Music and culture 
  11883 => 37, #11883: open culture 
  4964 => 37, #4964: arts 
  3898 => 37, #3898: art  
  /* Artificial intelligence (38) */
  8854 => 38, #8854:  artificial intelligence
  /* Big data (39) */
  4121 => 39, #4121: big data
  /* Biology (40) */
  6453 => 40, #6453: neurobiology
  3920 => 40, #3920: biology
  5211 => 40, #5211: Marine biology
  5250 => 40, # 5250: Microbiology
  10903 => 40, #10903: evolutionary and speculative biology
  10699 => 40, #10699: evolutionary biology
  11766 => 40, #11766: infection biology
  12799 => 40, #12799: speculative biology
  /* Biotechnology (41) */
  7210 => 41, #7210: biotechnology 
  12354 => 41, #12354: Nature Biotechnology
  9133 => 41, #9133: Biotechnology and Development Monitor
  /* Blogging (42) */
  4405 => 42, #4405: blogging
  10674 => 42, #10674: Science fiction & Travel blogging/vlogging
  /* Books/Literature (43) */
  5361 => 43, # Literatur
  15039 => 43, # literature and culture
  9835 => 43, # Literature and European Youth Culture
  4499 => 43, #4499: books 
  11993 => 43, #11993: books.
  3899 => 43, # 3899: literature
  9835 => 43, # 9835: Literature and European Youth Culture
  9867 => 43, #9867: Indian literature
  /* Border politics (44) */
  5344 => 44, #5344: border politics
  /* Business and Development (45) */
  11590 => 45, #11590: Business and development
  /* Cinema (49) */
  3883 => 49, #3883: cinema
  7791 => 49, #7791: film/cinema
  6390 => 49, #6390: cinematography
  /* Civil rights (48) */
  3931 => 48, #3931: civil rights"
  /* Climate change (50) */
  7022 => 50, #7022: climate change; sustainable development
  6624 => 50, #6624: climate change adaptation
  3934 => 50, #3934: climate change"
  /* Colonialism (51) */
  5568 => 51, #5568: postcolonialism
  5162 => 51, #5162: colonialism"
  /* Comics/Graphic novels (54) */
  6203 => 54, #6203: graphic novels and comics
  10906 => 54, #10906: comics"
  /* Conflict reporting (52) */
  9751 => 52, #9751: Conflict reporting
  /* Consumer rights (53) */
  5715 => 53, #5715: Consumer Rights
  /* Crime and corruption reporting (22) */
  6843 => 22, #6843: white colar crime
  6361 => 22, #6361: war crimes
  10872 => 22, #10872: Organised Crime; Human Rights
  6840 => 22, #6840: #organised crime
  6845 => 22, #6845: offshore tax crime
  11515 => 22, #11515: environmental crime ; ecology;
  6259 => 22, #6259: environmental crime
  8558 => 22, #8558: Crimean Tatars
  8557 => 22, #8557: Crimea
  7239 => 22, #7239: crime stories
  4427 => 22, #4427: crime
  6258 => 22, #6258: corporate crime
  4569 => 22, #4569: business crime
  12294 => 22, #12294: at risk youth and crime
  3930 => 22, # 3930: corruption 
  /* Darknet (57) */
  10645 => 57, #10645: Darknet
  /* Data journalism (24) */
  9759 => 24, #9759: Investigate and Data Journalism
  11684 => 24, #11684: Economy and data journalism
  4198 => 24, #4198: data journalism
  /* Design thinking (58) */
  9000 => 58, #9000: Design Thinking
  /* Development reporting (59) */
  12609 => 59, #12609: Development Reporting
  /* Digital life (60) */
  12028 => 60, #12028: Digital Life
  /* Diversity (61) */
  4682 => 61, #4682: diversity
  7114 => 61, #7114: cultural diversity
  /* Drugs (62) */
  10492 => 62, #10492: illegal drugs used as potential medicine	
  4770 => 62, #4770: drugs"
  /* Ecology (64) */
  10291 => 64, #10291: marine ecology
  3919 => 64, #3919: ecology
  7033 => 64, #7033: antarctic ecology
  6519 => 64, #6519: biodiversity
  /* Economy (63) */
  3975 => 63, #3975: Economy
  8286 => 63, #8286: Political Economy 
  8885 => 63, #8885: business economy
  7779 => 63, #7779: care economy
  7780 => 63, #7780: solidarity economy
  7538 => 63, #7538: economy policy
  8828 => 63, #8828: social economy
  8120 => 63, #8120: new forms of economy
  /* Education (65) */
  4134 => 65, #4134: higher education
  12347 => 65, #12347: education policy
  12142 => 65, #12142: Journalism Education
  6579 => 65, #6579: education.
  4082 => 65, #4082: education
  /* Encryption (66) */
  7613 => 66, #7613: encryption
  /* Entertainment (67) */
  4949 => 67, #4949: entertainment
  /* Environment (68) */
  6414 => 68, #6414: environmental issues
  11799 => 68, #11799: environment and development
  10286 => 68, #10286: marine environment
  12344 => 68, #12344: environment policy
  10842 => 68, #10842: Environmental Journalism
  9576 => 68, #9576: environment and politics
  12393 => 68, #12393: Environment. global development
  11668 => 68, #11668: the environment
  9637 => 68, #9637: Environment and development.
  9923 => 68, #9923: environment news
  4555 => 68, #4555: environmental science
  4570 => 68, #4570: environmental politics
  4076 => 68, #4076: environment
  /* Extremism (69)*/
  5649 => 69, #5649: extremism
  4487 => 69, #4487: right-wing extremism
  10953 => 69, #10953: countering violent extremism through media - Internet Governance
  6384 => 69, #6384: thematic priority political extremisms
  /* Fake news/fact-checking (70) */
  12842 => 70, #12842 Investigate fake news
  12683 => 70, #12683 Fact Checking
  /* Feminism/female empowerment (71) */
  3896 => 71, #3896 feminism
  7784 => 71, #7784 Feminismus
  12244 => 71, #12244 focusing on feminism
  6394 => 71, #6394 female empowerment
  /* Finance and banking (72)*/
  5377 => 72, #5377 finance
  4945 => 72, #4945 banking and finance
  10338 => 72, #10338 finance and insurance
  8652 => 72, #8652 private finance
  /* FoI / Freedom of Information requests (74) */
  9405 => 74, #9405: Freedom of Information (FOI) / Informationsfreiheitsgesetz (IFG)
  /* Food (73) */
  4618 => 73, #4618: food
  6992 => 73, #6992 (slow) food
  11871 => 73, #11871 Food and Environment
  5249 => 73, #5249 Food chemistry
  9158 => 73, #9158 Food and Beverage
  4049 => 73, #4049 food production
  5348 => 73, #5348 organic food
  6904 => 73, #6904 Food Security
  8957 => 73, #8957 food and travel
  /*75	Fundraising	*/
  14434 => 75, #14434 Fundraising
  /*76	Games/gaming */
  4366 => 76, #4366 computer games and weapons	
  4415 => 76, #4415 games	
  12363 => 76, #12363 serious games
  12364 => 76, #12364 audio games	
  7532 => 76, #7532 video games
  /*77	Genetic engineering*/
  7717 => 77, #7717 genetic engineering
  /*79	Geology*/
  4286 => 79, #4286 geology
  /*78	Geopolitics	*/
  9498 => 78, #9498 Conflict journalism - geopolitics	
  10414 => 78, #10414 commodities surveillance geopolitics	
  8432 => 78, #8432 Geopolitics of Energy	
  5877 => 78, #5877 geopolitics
  /*80 Globalization*/
  4226 => 80, #4226 globalization
  /*81	Governance */
  8195 => 81, #8195 governance	
  11930 => 81, #11930 eGovernance	
  9381 => 81, #9381 environment transparency governance	
  8406 => 81, #8406 good governance
  /*82	Health	"*/
  12807 => 82, #12807 Healthcare	
  5414 => 82, #5414 sexual health	
  11055 => 82, #11055 health and public politics	
  6454 => 82, #6454 global health)	
  3906 => 82, #3906 health policy	
  3918 => 82, #3918 health	
  7758 => 82, #7758 health care	
  4190 => 82, #4190 global health	
  4191 => 82, #4191 public health	
  12127 => 82, #12127 Science and Health	
  9828 => 82, #9828 mental health	
  8550 => 82, #8550 outdoor health	
  10919 => 82, #10919 Womens health	
  10156 => 82, #10156 Gender and health issues	
  8399 => 82, #8399 Health and science
  11227 => 82, #11227 health system
  /*83	History	*/
  4365 => 83, #4365 human history	
  4887 => 83, #4887 Media History	
  9495 => 83, #9495 Contemporary History	
  4897 => 83, #4897 Art History	
  5454 => 83, #5454 travel history	
  7524 => 83, #7524 modern European history	
  3944 => 83, #3944 history	
  4206 => 83, #4206 global history	
  7544 => 83, #7544 Russia: history	
  4795 => 83, #4795 German history	
  5569 => 83, #5569 history of berlin	
  8160 => 83, #8160 history of Belarus	
  8161 => 83, #8161 history of Russia	
  8162 => 83, #8162 History of Poland	
  /*84	Hostile environments*/
  6833 => 84, #6833 Hostile Environments	1
  /*86	Human rights	"*/
  3941 => 86, #3941 human rights	
  6899 => 86, #6899 human rights issues	
  6655 => 86, #6655 human rights; immigration
  /*26	Immersive journalism (3D/360°)*/
  10194 => 26, #10194 feature writing: immersive journalism; long term
  /*90	Inclusion	*/ 
  9882 => 90, #9882 inclusión social
  5113 => 90, #5113 inclusion
  /* 87	Innovation	*/
  12547 => 87, #12547 Journalism Innovation	
  7177 => 87, #7177 innovation research
  12081 => 87, #12081 Media innovation & startups
  11885 => 87, #11885 social innovation	
  3973 => 87, #3973 Innovation	
  9119 => 87, #9119 inclusive innovation	
  12213 => 87, #12213 Innovation in journalism (education)
  /*89	International relations*/
  12140 => 89, #12140 .International Relations	
  12166 => 89, #12166 National & International relations	
  12178 => 89, #12178 International Relations; Media related topics	
  5061 => 89, #5061 International Relations
  /*25	Investigative journalism*/
  10532 => 25, #10532 local investigative journalism	
  3991 => 25, #3991 investigative journalism
  9917 => 25, #9917 science and investigative journalism
  12734 => 25, #12734 Investigative Journalism; Human Stories
  10206 => 25, #10206 Investigative journalism on migration
  /*91	Justice and law	"*/
  9858 => 91, #9858  criminal & social justice	
  10116 => 91, #10116 Global Injustice	
  6554 => 91, #6554 social justice	
  8383 => 91, #8383 justice system	
  6097 => 91, #6097 Justice	
  5335 => 91, #5335 global justice	 10752 antidiscrimination law
  12090 => 91, #12090 international law and cultural resistance
  6745 => 91, #6745 international law	
  8107 => 91, #8107 rule of law	
  5840 => 91, #5840 media law	
  4058 => 91, #4058 law	
  5341 => 91, #5341 laws	
  5886 => 91, #5886 EU law	
  10750 => 91, #10750 electoral law	
  5887 => 91, #5887 criminal law	
  10751 => 91, #10751 public international law
  /*92	Labor market	*/
  7470 => 92, #7470 labor market	1
  /*93	LGBTQ	"*/
  11648 => 93, #11648 LGBTQ	
  12462 => 93, #12462 LGBTQIA
  /*94	Lifestyle	"*/
  4697 => 94, #4697 lifestyle	
  11222 => 94, #11222 hlifestyle	
  4066 => 94, #4066 lifestyle phenomena	
  9442 => 94, #9442 expat lifestyle	 7913 green lifestyle	
  11256 => 94, #11256 Digital Lifestyle	
  /*95	Lobbyism	*/
  3997 => 95, #3997 lobbyism
  /*27	Local journalism	*/
  5990 => 27, #5990 local journalism and history
  5536 => 27, #5536 local journalism
  /*96	Mafia/organized crime	"*/
  8742 => 96, #8742 mafiasocial issues	
  5104 => 96, #5104 mafia	
  4114 => 96, #4114 organized crime
  /*97	Media freedom	*/
  8395 => 97, #8395 media freedom
  /*98	Medicine	"*/
  4101 => 98, #4101 medicines	
  3993 => 98, #3993 medicine	
  4768 => 98, #4768 alternative medicine	
  5046 => 98, #5046 complementary medicine
  4853 => 98, #4853 herbal medicine	
  11765 => 98, #11765 telemedicine
  /*99	Migration and refugees	*/
  11759 => 99, #11759 Migration and Refugees
  /*28	Mobile journalism	*/
  4216 => 28, #4216 mobile journalism		1
  /*100 Music	"*/ 
  4369 => 100, #4369 reggae music	
  7499 => 100, #7499 music business	
  4173 => 100, #4173 music	
  5788 => 100, #5788 jazz music	
  6308 => 100, #6308 electronic music	
  7869 => 100, #7869 classical music	
  4822 => 100, #4822 world music	
  4823 => 100, #4823 global music	
  4824 => 100, #4824 experimental music	
  4828 => 100, #4828 musicology
  /*101	Net neutrality	"*/
  4019 => 101, #4019 net neutrality	
  4585 => 101, #4585 net neutrality civil rights
  /*102	NGOs	*/
  11957 => 102, #11957 NGOs		1
  /*29	Nonprofit journalism	*/ 
  7268 => 29, #7268 nonprofit journalism		1
  /*103	Open source	*/
  7509 => 103, #7509 open source
  104 => 103, #104	Peace building	
  6501 => 103, #6501 peace building.	
  8117 => 103, #8117 peace building	
  /*105	Podcasts	*/
  10667 => 105, #10667 podcasts	3	1
  /*107	Religion	"*/
  10037 => 107, #10037 Religion journalism	
  4489 => 107, #4489 religion and politics	
  4246 => 107, #4246 religion	
  /*30	Science journalism	*/
  5225 => 30, #5225 science journalism		1
  8215 => 30, #science; travel; finance (u.s. citizen martin fran 
  14459 => 30, #Scientific 
  4754 => 30, #scientific publications 	
  5733 => 30, #scientific topics
  3917 => 30, #science
  5770 => 30, #science & nature 
  8036 => 30, #science (marine ressources) 
  5736 => 30, #science (primary) 
  14222 => 30, #science and environment
  10250 => 30, #Science and environment broadcaster and writer 
  14214 => 30, #science and medical journalism 
  7698 => 30, #Science and Society 
  12189 => 30, #Science and tech writer with a broad scope of topics.
  14119 => 30, #science and technology 
  14461 => 30, #Science and Travel
  5210 => 30, #Science Comedy
  4074 => 30, #science communication
  8822 => 30, #science communication: anything from space to archeology
  12119 => 30, #Science communication; Social Sciences
  5602 => 30, #science for children
  7166 => 30, #science in general
  10904 => 30, #science miscellanea
  12343 => 30, #science policy
  7714 => 30, #science politics
  9307 => 30, #science reporting
  11435 => 30, #Science Storytelling in Spanish
  15049 => 30, #Science Technology Business
  4998 => 30, #science: from archeology to neuroscience
  8541 => 30, #science; travel
  7234 => 30, #Science; travel; Culture; Music
  8971 => 30, #science; travel; culture; music; philosophy
  /*108	Sexuality and gender*/
  5411 => 108, #5411 sexuality	
  10905 => 108, #10905 gender and sexuality	
  10665 => 108, #10665 Gender & Sexuality
  8968 => 108, #  sex
  13453 => 108, #Sex work
  5111 => 108, #sexual and reproductive rights and health
  6861 => 108, #sexual violence
  /*109	Social movements*/
  5346 => 109, #5346 social movements		1
  /*113	Sports	*/
  4282 => 113, #4282 sports	
  4797 => 113, #4797 extrem sports	
  /*110	Startups	*/
  3972 => 110, #3972 Startups	
  4848 => 110, #4848 IT startups	
  /*111	Surveillance	*/
  3998 => 111, #3998 surveillance		1
  /*112	Sustainability	*/
  4680 => 112, #4680 sustainability	
  10481 => 112, #10481 Sustainability and Digitization
  /*114	Technology*/
  3921 => 114, #3921 technology	
  8563 => 114, #8563 technology of information and communication	
  4212 => 114, #4212 information technology	
  4213 => 114, #4213 communication technology
  9080 => 114, #9080 process technology	
  5248 => 114, #5248 Food technology
  10369 => 114, #10369 media and technology	
  11915 => 114, #11915 technology issues
  5550 => 114, #5550 technology travel	
  11958 => 114, #11958 environment technology	
  5313 => 114, #5313 new technology	
  /*115	Terrorism*/
  10017 => 115, #10017 anti terrorism	
  6797 => 115, #6797 terrorism	
  6609 => 115, #6609 Islamic and international Terrorism
  /*118	Tourism	*/
  7465 => 118, #7465 tourism-industry	
  4673 => 118, #4673 tourism	
  5511 => 118, #5511 Tourism.
  11736 => 118, #11736 Sustainable Tourism
  /*116	Trade	*/
  3911 => 116, #3911 trade	
  3943 => 116, #3943 trade unions	
  6903 => 116, #6903 Fair Trade
  7160 => 116, #7160 world trade
  /*117	Transparency*/
  8202 => 117, #8202 transparency
  /*31	Travel journalism	*/
  5677 => 31, #5677 travel report
  12104 => 31, #12104 Travel Writer	
  7837 => 31, #7837 travel writing
);

