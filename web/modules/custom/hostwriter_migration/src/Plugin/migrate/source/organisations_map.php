<?php

return array(
  /* 122 AG Dok*/
  8817 => 122,
  7521 => 122,
  /* 128  Arab Reporters for Investigative Journalism (ARIJ) */
  11630 => 128,
  /* 134 Asociación Española de Comunicación Científica (AECC) */
  10176 => 134, 
  11245 => 134,
  /* 141 Bayerischer Journalistenverband (BJV) */
  8791 => 141,
  6172 => 141,
  4070 => 141,
  /* 142 Belarusian Association of Journalists (BAJ) */
  8165 => 142,
  8802 => 142,
  /* 143 Berliner Journalistenschule (BJS) */
  5811 => 143,
  8827 => 143,
  /* 149 Café Babel */
  9542 => 149, 
  8422 => 149, 
  11371 => 149,
  /* 152 Caravan’s Journal */
  9394 => 152,
  /* 160 Code for Africa */
  12586 => 160,
  /* 162 Committee to Protect Journalists */ 
  9515 => 162,
  /* 164 Correctiv */
  7630 => 164, 
  8846 => 164, 
  7727 => 164,
  /* 165 Consortium for Press Freedom (CPF) */
  7629 => 165, 
  8855 => 165,
  /* 170 Dart Center for Journalism and Trauma */
  9125 => 170, 
  6867 => 170,
  /* 172 Deutsch-Chinesisches Mediennetzwerk */
  12234 => 172,
  /* 173 Deutsche Gesellschaft für Auswärtige Politik (DGAP) */
  12676 => 173,
  /* 175 Deutsche Journalisten Union (DJU) */ 
  4549 => 175,
  8768 => 175,
  10329 => 175,
  8811 => 175, 
  8779 => 175, 
  9589 => 175, 
  8812 => 175, 
  6533 => 175, 
  6386 => 175,
  /* 176 Deutscher Journalisten-Verband (DJV)*/
  11786 => 176, 
  11475 => 176, 
  5685 => 176,
  /* 177 Deutsche Journalistenschule (DJS)*/ 
  5091 => 177, 
  11428 => 177,
  /*178 Deutscher Fachjournalisten Verband (DFJV)*/
  8804 => 178, 
  5516 => 178, 
  4100 => 178,
  11963 => 178,
  /* 180 Digital Media Women (DMW) */
  6346 => 180,
  /*183 Doha Centre for Media Freedom*/
  8397 => 183,
  /* 184 Menell Media Exchange */
  3925 => 184, 
  8570 => 184,
  /*186 Earth Journalism Network (EJN)*/
  9678 => 186, 
  12264 => 186,
  /*188 Electronic Media School (EMS)*/
  7122 => 188, 
  8835 => 188, 
  8755 => 188,
  /*189 Erasmus Mundus*/ 
  8736 => 189, 
  12085 => 189,
  /* 192 European Forum Alpbach */
  10741 => 192,
  /*194 European Journalism Training Association (EJTA)*/
  12215 => 194, 
  11457 => 194,
  /*195 European Youth Press */
  7863 => 195, 
  6253 => 195, 
  6626 => 195,
  /*198 Fixers.press */
  12084 => 198, 
  11487 => 198,
  /*202 Frauennetzwerk Medien*/
  11042 => 202,
  /*203 Freelens e.V.*/ 
  5117 => 203, 
  4787 => 203, 
  7911 => 203,
  /*204 Freischreiber e.V.*/ 
  9588 => 204, 
  3927 => 204, 
  11999 => 204, 
  11998 => 204, 
  7724 => 204,
  /* 207 Ghana Journalists Association (GJA) */
  9452 => 207,
  /*211 Hacks/Hackers */
  10836 => 211, 
  10200 => 211,
  /*213 Hong Kong Journalists Association*/
  9535 => 213, 
  9530 => 213,
  /*223 International Consortium of Investigative Journalists (ICIJ)*/
  5843 => 223, 
  8210 => 223,
  /*224 International Federation of Journalists (IFJ)*/
  4532 => 224, 
  6503 => 224, 
  6496 => 224, 
  8166 => 224, 
  4485 => 224,
  /*225 International Journalists' Programmes (IJP)*/
  4279 => 225, 
  4145 => 225, 
  4059 => 225, 
  6806 => 225, 
  4432 => 225, 
  3905 => 225, 
  4071 => 225, 
  11779 => 225, 
  6813 => 225, 
  8496 => 225, 
  8096 => 225,
  /*226 International Journalists Network (IJNet) */
  4394 => 226, 
  8149 => 226, 
  5513 => 226,
  /*227 International Press Institute (IPI)*/
  10273 => 227, 
  5927 => 227,
  /*228 International Reporters and Editors (IRE)*/
  4005 => 228, 
  10171 => 228, 
  3970 => 228, 
  11685 => 228, 
  12367 => 228,
  /*234 Investigative Reporting Project Italy (IRPI)*/
  233 => 234, 
  6200 => 234, 
  10208 => 234, 
  9585 => 234,
  /*238 Journalism Academy of Konrad-Adenauer-Foundation (KAS)*/
  8808 => 238, 
  7243 => 238,
  8841 => 238,
  5097 => 238, 
  5007 => 238, 
  4138 => 238, 
  8778 => 238, 
  10351 => 238,
  8815 => 238,
  /*239 Journalism and Women Symposium (JAWS)*/
  6787 => 239,
  /*241 Journalistenverband Berlin-Brandenburg (JVBB)*/
  4155 => 241,
  8836 => 241, 
  3904 => 241,
  6093 => 241,
  /*244 Journalists Helping Journalists*/ 
  10404 => 244,
  /*245 Journalists’ Union of Turkey (TGS)*/ 
  11376 => 245,
  /*240 Journalists Network*/
  9124 => 240, 
  3893 => 240, 
  8139 => 240, 
  8762 => 240, 
  8795 => 240,
  /*246 Journocode*/
  9386 => 246,
  /*248 German Youth Press (Jugendpresse Deutschland)*/
  9189 => 248, 
  4407 => 248,
  4137 => 248, 
  4406 => 248, 
  9272 => 248, 
  9255 => 248,
  /*249 Young Journalists (Junge Journalisten)*/
  4014 => 249,
  11354 => 249, 
  4374 => 249, 
  7778 => 249,
  /*251 Krautreporter*/
  4926 => 249,
  /*261 National Association of Black Journalists*/ 
  5989 => 261,
  /*262 National Association of Hispanic Journalists (NAHJ)*/ 
  8916 => 262, 
  6818 => 262, 
  10724 => 262,
  /*263 National Association of Science Writers (NASW)*/ 
  9872 => 263, 
  12278 => 263, 
  7085 => 263,
  /*265 National Press Photographers Association*/
  10942 => 265, 
  8356 => 265,
  /*266 National Union of Journalists UK & Ireland (NUJ)*/
  8666 => 266, 
  11672 => 266,
  9755 => 266, 
  10869 => 266,
  11605 => 266,
  /*268 Dutch Union of Journalists (NVJ)*/ 
  6517 => 268,
  6770 => 268, 
  6434 => 268, 
  11659 => 268,
  /*269 Network for Reporting on Eastern Europe (n-ost)*/ 
  4237 => 269,
  10469 => 269,
  7743 => 269,
  7310 => 269,
  /*271 Netzwerk Recherche*/ 
  3940 => 271, 
  7204 => 271, 
  /*272 Neue deutsche Medienmacher*/ 
  4299 => 272, 
  5299 => 272,
  /*273 Nigerian Union of Journalists (NUJ)*/ 
  10996 => 273, 
  12338 => 273, 
  11044 => 273, 
  11459 => 273,
  /*274 Norwegian Union of Journalists (NJ)*/ 
  4119 => 274,
  /*275 Organized Crime and Corruption Reporting Project (OCCRP)*/
  8211 => 274,
  /*277 Outriders Network*/
  11856 => 277,
  /*278 P24 - Platform for Independent Journalists*/
  7074 => 278,
  /*279 Panhellenic Union of Sports Journalists  (PSAT)*/
  6468 => 279,
  /*280 Philippine Science Journalists Association */
  12257 => 280, 
  7212 => 280,
  /*281 Polish Journalists Association  (SDP)*/
  9816 => 281,
  /*284 Pressenetzwerk für Jugendthemen (PNJ)*/ 
  5796 => 284, 
  5118 => 284,
  /*286 Pune Union of Working Journalists */
  3964 => 286,
  /*288 Reporters Without Borders*/
  9302 => 288, 
  8533 => 288, 
  4004 => 288,
  7000 => 288, 
  4743 => 288,
  /*289 RiffReporter*/
  11296 => 289,
  /*291 Robert Bosch Media Fellow*/ 
  11578 => 291, 
  12235 => 291, 
  8660 => 291, 
  5867 => 291, 
  9320 => 291, 
  10792 => 291,
  /*294 SCOOP */
  11110 => 294,
  /*295 Seoul Foreign Correspondents Club */
  7674 => 295,
  /*297 Society of Environmental Journalists*/
  6524 => 297,
  /*298 Society of Professional Journalists  (SPJ)*/
  12287 => 298, 
  5481 => 298, 
  6486 => 298,
  /* 300 Sonderland*/ 
  11347 => 300,
  /*301 South African Freelancers’ Association (SAFREA)*/
  11694 => 301,
  /*303 South East Europe Media Organisation (SEEMO)*/ 
  10703 => 303, 
  8212 => 303,
  /*306 PEN */ 
  11375 => 306,
  12290 => 306,
  /*308 Syndicat National des Journalistes du Cameroun (SNJC)*/ 
  8155 => 308,
  /*309 Syrian Investigative Reporting for Accountability Journalism (SIRAJ)*/ 
  11631 => 309,
  /*321 Uganda Women Writers Association (FEMRITE)*/ 
  11724 => 321,
  /*323 Unión de Periodistas de Cuba (UPEC)*/
  8302 => 323,
  /*324 Union Internationale de la Presse Francophone (UPF)*/
  11415 => 324, 
  11025 => 324,
  /*328 Verband Deutscher Agrarjournalisten (VDAJ)*/
  4672 => 328,
  /*329 Ver.di*/
  4430 => 329, 
  7518 => 329, 
  4095 => 329, 
  7753 => 329,
  /*331 Verein der Ausländischen Presse in Deutschland (VAP)*/ 
  8820 => 331,
  5905 => 331,
  /*333 Vereinigung Deutscher Reisejournalisten (VDRJ)*/ 
  5681 => 333, 
  10242 => 333,
  /*335 Vocer*/
  4902 => 335, 
  8814 => 335,
  3894 => 335,
  /*338 Vlaamse Vereniging voor Journalistiek (VVJ)*/ 
  7141 => 338, 
  10454 => 338,
  8243 => 338, 
  10495 => 338,
  /*339 Dutch Flemish Association for Investigation (VVOJ)*/ 
  8601 => 339,
  /*342 Weltreporter */
  8988 => 342,
  /*343 Wissenschaftspressekonferenz (WPK)*/ 
  4013 => 343, 
  3924 => 343, 
  11389 => 343, 
  5168 => 343, 
  11393 => 343, 
  4107 => 343, 
  8764 => 343, 
  9157 => 343, 
  4120 => 343, 
  12146 => 343, 
  7699 => 343, 
  4154 => 343, 
  5229 => 343,
  /*344 Women in Film and Television (WIFTG)*/
  6673 => 344,
  /*345 Women In Media*/
  5416 => 345,
  /*347 World Federation of Science Journalists (WFSJ)*/ 
  12256 => 347,
  7889 => 347,
  /*349 World Press Institute*/
  6516 => 349,
  /*350 Writers Federation of Nova Scotia*/ 
  6777 => 350,
  /*351 Writers Guild of Canada*/
  6778 => 351,
  /*352 Writers Union of Canada*/ 
  6776 => 352,
  /*356 Youth Press Association of Bosnia and Herzegovina */ 
  6319 => 356,
  /*357 Zahnräder Netzwerk*/ 
  11447 => 357,
  /*358 Zambian Bloggers Network*/
  10350 => 358,
  /*361 Association for Education in Journalism and Mass Communication (AEJMC)*/
  9738 => 361,
  /*362 African Media Initiative (ami)*/ 
  8565 => 362,
  /*363 Association of Health Care Journalists (AHCJ)*/
  9676 => 363,
  /*364 Association Internationale de la Presse Sportive (AIPS)*/
  99999 => 364,
  /*365 Association des Journalistes Scientifiques de la Presse d'Information (AJSPI)*/
  5965 => 365, 
  7171 => 365,
  /*366 Anglo American Press Association (AAPA)*/ 
  10718 => 366,
  /*367 Asian American Journalists Association (AAJA)*/ 
  7733 => 367,
  /*368 Asian European Media Network (AEMNet)*/ 
  9629 => 368,
  /*369 Cameroon Association of English Speaking journalist*/ 
  9591 => 369,
  /*370 Association of Caribbean Media Workers (ACM)*/
  10337 => 370,
  /*371 Confederation of Progressive Unions of Turkey (DISK)*/ 
  12722 => 371,
  /*372 Danish Journalist Union*/
  8633 => 372,
  5834 => 372,
  5622 => 372,
  5628 => 372,
  /* 373 European Journalism Center (EJC)*/
  5757 => 373,
  /*374 Foreign Correspondents' Association*/
  11565 => 374, 
  5340 => 374, 
  10801 => 374, 
  9963 => 374, 
  7222 => 374,
  10039 => 374,
  8670 => 374, 
  9776 => 374,
  /*375 Frontline Club*/ 
  8199 => 375,
  /*376 HackPack*/
  10671 => 376,
  /*377 Internationale Medienhilfe (IMH)*/
  7628 => 377,
  /*378 International Womens' Media Foundation (IWMF)*/
  11366 => 378,
  /*380 Kenya Film and Television Professionals Association (KFTPA)*/
  8911 => 380,
  /*381 Kenya Union of Journalists (KUJ)*/
  10022 => 381,
  /*382 Media Institute of Southern Africa (MISA)*/ 
  9714 => 382, 
  10352 => 382,
  /*383 Network of Women in Media, India (NWMI)*/
  9849 => 383,
  /*384 National  Union Of Somali Journalist (NUSOJ)*/
  9813 => 384, 
  9866 => 384,
  /*385 Ordine dei giornalisti*/
  7923 => 385, 
  9590 => 385, 
  8491 => 385,
  /*388 Peshawar Press Club (PPC)*/
  9234 => 388,
  /*389 Pro Quote */
  5425 => 389,
  /*390 Rede Brasileira de Jornalismo Ambiental (RJBA)*/
  7007 => 390,
  /*391 Romanian Centre for Investigative Reporting (CRJI)*/ 
  10222 => 391,
  /*392 Sierra Leone Association of Journalists (SLAJ)*/ 
  12646 => 392,
  /*393 Sierra Leone Reporters Union (SLRU)*/ 
  12633 => 393,
  /*394 Swedish Association of Journalists (SJF)*/ 
  4533 => 394,
  /*395 The Alliance of Independent Journalists Indonesia (AJI)*/ 
  8008 => 395,
  /*396 The European Federation of Journalists (EFJ)*/ 
  8167 => 396,
  /*397 Zeitenspiegel-Reportageschule*/ 
  8222 => 397, 
  5918 => 397,
  /*399 Philippine Press Institute (PPI)*/
  7953 => 399,
);
