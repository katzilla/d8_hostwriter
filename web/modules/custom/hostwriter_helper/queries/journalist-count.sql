select sum(journalists),count(country)
from
  (
    select count(users.uid) journalists, location.field_user_location_country_code country
    from users_field_data users
           inner join user__field_user_location location on users.uid = location.entity_id and location.deleted = 0
           inner join user__roles roles
                      on roles.entity_id = users.uid and roles.roles_target_id = 'hostwriter' and roles.deleted = 0

    where status = 1
    group by country
  ) count