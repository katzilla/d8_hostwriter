<?php

/**
 * @file
 * Primary module hooks for hostwriter_helper module.
 *
 * @DCG
 * This file is no longer required in Drupal 8.
 * @see https://www.drupal.org/node/2217931
 */

use Drupal\Core\Entity\EntityInterface;

/**
 * Remove drupal-routes from mailtext for personal contact form messages
 *
 * @param $message
 */
function hostwriter_helper_mail_alter(&$message) {

  if ($message['module'] == 'contact' && isset($message['params']['contact_message'])) {
    $contact_message = $message['params']['contact_message'];
    $sender = $message['params']['sender'];
    $language = \Drupal::languageManager()->getLanguage($message['langcode']);

    $variables = [
      '@site-name' => \Drupal::config('system.site')->get('name'),
      '@subject' => $contact_message->getSubject(),
      '@form' => !empty($message['params']['contact_form']) ? $message['params']['contact_form']->label() : NULL,
      '@form-url' => \Drupal::url('<current>', [], ['absolute' => TRUE, 'language' => $language]),
    ];
    if ($sender->isAuthenticated()) {

      $variables['@sender-url'] = "https://hostwriter.org" . $sender->url('canonical', ['absolute' => FALSE, 'language' => $language]);
      $variables['@sender-name'] = $sender->field_user_firstname->value;
    }
    else {
      $variables['@sender-url'] = $message['params']['sender']->getEmail();
      $variables['@sender-name'] = $sender->getDisplayName();
    }

    $options = ['langcode' => $language->getId()];

    if ($message['key'] === 'user_mail' || $message['key'] === 'user_copy') {
      $recipient = $message['params']['recipient'];
      $variables += [
        '@recipient-name' => $recipient->field_user_firstname->value,
        '@recipient-edit-url' => $message['params']['recipient']->url('edit-form', ['absolute' => TRUE, 'language' => $language]),
      ];
      $message['body'] = [];
      $message['body'][] = t('Hello @recipient-name,', $variables, $options);
      $message['body'][] = t("@sender-name (@sender-url) has sent you a message via your personal contact form at @site-name:", $variables, $options);
      
      $build = entity_view($contact_message, 'mail');
      $message['body'][] = \Drupal::service('renderer')->renderPlain($build);
      $message['body'][] = "<br><br><hr>";
      $message['body'][] = t("To anwser to @sender-name just hit reply. \<br\> For privacy reasons no mails are stored on hostwriter.org", $variables, $options);
      $message['body'][] = "<br><br><hr>";
    }
  }
}

/**
 * Implements hook_views_query_alter().
 */
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\query\QueryPluginBase;

function hostwriter_helper_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  switch ($view->storage->id()) {
    case 'users';
      $query->addField('', 'uid', '', ['function' => 'groupby']);
      $query->addGroupBy('uid');
      break;
  }
}

/**
 * @param \Drupal\Core\Entity\EntityInterface $entity
 * Saves email address to username fields on save, updates full names and hostwire username
 */
function hostwriter_helper_entity_presave(EntityInterface $entity) {
  if (is_object($entity) && $entity->getEntityTypeId() === 'user') {
    $entity->name->value = $entity->mail->value;
    $entity->field_user_full_name->value = $entity->field_user_firstname->value . ' ' . $entity->field_user_lastname->value;
    $entity->field_user_hostwire_name->value = $entity->field_user_firstname->value . ' ' . $entity->field_user_lastname->value;

    /**
     * Set field activation date when status changes to active
     */
    if($entity->original) {
      $original_status = $entity->original->get('status');
      $status = $original_status->get(0);
      if (!empty($status)) {
        $new_status = $entity->get('status')->get(0)->value;
        if ($status->value == "0" && $new_status == "1") {
          $entity->field_activation_date = time();
        }
      }
    } 
    
    /* remove duplicates from term-mapping after migration 
     * @todo: remove after migration ist finished 
    */
    $expertise_raw = $entity->get('field_user_expertise')->getValue();
    $expertise  = _unique_key($expertise_raw,'target_id');
    $entity->field_user_expertise->setValue($expertise);


    /* remove duplicates from term-mapping after migration 
     * @todo: remove after migration ist finished 
    */
    $organisations_raw = $entity->get('field_user_organisations')->getValue();
    $organisations  = _unique_key($organisations_raw,'target_id');
    $entity->field_user_organisations->setValue($organisations);

    /* remove duplicates from worksamples links after migration 
     * @todo: remove after migration ist finished 
    */
    $links_raw = $entity->get('field_user_worksamples_link')->getValue();
    $links  = _unique_key($links_raw,'uri');
    $entity->field_user_worksamples_link->setValue($links);
  }
}

function _unique_key($array,$keyname){
  $new_array = array();
  foreach($array as $key=>$value){
    if(!isset($new_array[$value[$keyname]])){
      $new_array[$value[$keyname]] = $value;
    }
  }
  $new_array = array_values($new_array);
  return $new_array;
}
