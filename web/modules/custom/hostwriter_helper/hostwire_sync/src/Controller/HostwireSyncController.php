<?php

namespace Drupal\hostwire_sync\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\hostwire_sync\UserEvent;
use Drupal\Core\Config\ConfigFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Discourse Sync controller.
 */
class HostwireSyncController extends ControllerBase {

  const DISCOURSE_USER_CREATED_EVENT = 'user_created';

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;
  protected $webhookSecret;
  private $postHandler;

  public function __construct(EventDispatcherInterface $eventDispatcher,
    ConfigFactory $config, $postHandler) {
    $this->eventDispatcher = $eventDispatcher;
    $this->webhookSecret = $config->get('hostwire_sync.settings')->get('webhook_secret');
    $this->postHandler = $postHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('config.factory'),
      $container->get('hostwire_sync.get_posts')
    );
  }
  
  public function userWebhook() {
    $request = \Drupal::request();
    $discourse_event = $request->headers->get('x-discourse-event');
    if ($discourse_event !== self::DISCOURSE_USER_CREATED_EVENT) {
      return [];
    }

    $discourse_event_signature = $request->headers->get('x-discourse-event-signature');
    $discourse_payload_raw = file_get_contents('php://input');
    $signature = 'sha256=' . hash_hmac('sha256', $discourse_payload_raw, $this->webhookSecret);
    if ($signature !== $discourse_event_signature) {
      return [];
    }

    $payload = json_decode($discourse_payload_raw, TRUE);
    $username = $payload['user']['username'];
    $event = new UserEvent(user_load_by_name($username));
    $event = $this->eventDispatcher->dispatch(UserEvent::EVENT, $event);
    
    return [];
  }

  /**
   * Gets all posts and categories and puts them together to one neat array.
   * 
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function getPosts() {
    if (\Drupal::currentUser()->isAuthenticated()) {
      $handler = $this->postHandler;
      $posts = $handler->getPosts();
      if ($posts) {
        $categories = $handler->getCategories();
        $topic_list = $posts["topic_list"]["topics"] ? $posts["topic_list"]["topics"] : null;
        $category_list = $categories["category_list"]["categories"];

        foreach ($topic_list as $key => $topic) {
          $category_id = $topic["category_id"];
          $category = $this->getCategorybyId($category_list, $category_id);
          if ($category["read_restricted"] === true) {
            unset($topic_list[$key]);
          }
          else {
            $topic_list[$key]["category"] = $category;
          }
        }
        $topic_list = array_slice($topic_list, 0, 3, false);
        return new JsonResponse($topic_list);
      }
      else {
        return new JsonResponse(['no posts']);
      }
    }
    else {
      return new JsonResponse(['no posts']);
    }
  }


  /**
   * Helper function to get category from ID
   * @param $category_list
   * @param $id
   *
   * @return mixed
   */
  public function getCategorybyId($category_list, $id) {
    foreach ($category_list as $category) {
      if($category["id"] === $id) {
        return $category;
      }
    }
  }
}
