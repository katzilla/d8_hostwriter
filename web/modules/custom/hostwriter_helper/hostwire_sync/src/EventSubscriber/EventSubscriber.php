<?php

namespace Drupal\hostwire_sync\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\hostwire_sync\UserEvent;

/**
 * EventSubscriber class
 */
class EventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[UserEvent::EVENT][] = ['onUserCreate', 0];
    return $events;
  }
  
  /**
   * Handler for the hostwire_sync user event.
   *
   * @param \Drupal\hostwire_sync\UserEvent $event
   */
  public function onUserCreate(UserEvent $event) {
    $service = \Drupal::service('hostwire_sync.group');
    $service->syncUserGroups($event->getUsername(), $event->getUserRoles());
  }
}
