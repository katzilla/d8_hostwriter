<?php

namespace Drupal\hostwire_sync;

use \Drupal\discourse_sso\SingleSignOnBase;

/**
 * Synchronize Drupal user groups (partner taxonomy on user profiles) 
 * to discourse groups.
 * 
 * @todo: implement groups on user create (see event subscriber)
 */
class Group extends SingleSignOnBase {
  
  /**
   * Gets a user and checks field_hostwire_groups for assigned groups (taxonomy term reference field).
   * Syncs groups with existing groups in Hostwire on save.
   * see: https://meta.discourse.org/t/sync-sso-user-data-with-the-sync-sso-route/84398
   * 
   * @param $user_entity
   */
  public function syncUserGroups($user_entity) {

    $groups = [];
    $partners = $user_entity->field_hostwire_groups->referencedEntities();
    foreach ($partners as $term) {
      $groups[]= $term->field_hostwire_group_id->value;
    }

    $hostwire_user = $this->getHostwireUser($user_entity);
    if (!$hostwire_user) {
      $this->createHostwireUser($user_entity, $groups);
    } 
    else {
      $hostwire_groups = $this->getHostwireUserGroups($hostwire_user);

      // New groups
      $new = array_diff($groups, $hostwire_groups);
      foreach ($new as $group) {
        $this->addUserToHostwireGroup($hostwire_user, $group);
      }

      // Removed groups
      $obsolete = array_diff($hostwire_groups, $groups);
      foreach ($obsolete as $group) {
        $this->removeUserFromHostwireGroup($hostwire_user, $group);
      }
    }
    return;
  }

  /**
   * @param $hostwire_user
   * @param $group
   * @param string $method
   *
   * @return array
   */
  public function addUserToHostwireGroup($hostwire_user, $group, $method = 'PUT') {
    $gid = $this->getGroupIdByName($group);
    if (!$gid) {
      $message = $this->t('Discourse not synchronized.');
      $message .= ' ' . $this->t('Group @group does not exist.', ['@group' => $group]);
      \Drupal::logger('hostwire_sync')->notice($message);
      return null;
    }

    $url = $this->url . '/groups/' . $gid . '/members.json';
    $parameters = [
        'json' => [
          'usernames' => $hostwire_user->user->username,
        ]
      ] + $this->getDefaultParameter();
    try {
      $request = $this->client->request($method, $url, $parameters);
      $response = json_decode($request->getBody());

      if ($response && key($response) === 'success') {
        $message = $this->t('Discourse user @username: assigned to @group.', [
          '@username' => $hostwire_user->user->username,
          '@group' => $group
        ]);
        \Drupal::logger('hostwire_sync')->notice($message);
      }
      else {
        \Drupal::logger('hostwire_sync')->notice('could not assign group ' . $group . ' to user: ' . $hostwire_user->user->username);
      }
      return $response;
    }
    catch (\GuzzleHttp\Exception\GuzzleException $e) {
      watchdog_exception('hostwire_sync', $e, $e->getMessage());
    }
    return null;
  }

  /**
   * @param $hostwire_user
   * @param $group
   * @param string $method
   *
   * @return array
   */
  public function removeUserFromHostwireGroup($hostwire_user, $group, $method = 'DELETE') {
    $gid = $this->getGroupIdByName($group);
    if (!$gid) {
      $message = $this->t('Discourse not synchronized.');
      $message .= ' ' . $this->t('Group @group does not exist.', ['@group' => $group]);
      \Drupal::logger('hostwire_sync')->notice($message);
      return null;
    }

    $url = $this->url . '/groups/' . $gid . '/members.json';
    $parameter = [
        'json' => [
          'user_id' => $hostwire_user->user->id
        ]
      ] + $this->getDefaultParameter();
    try {
      $request = $this->client->request($method, $url, $parameter);
      if ($request->getStatusCode() === 200) {
        $response = json_decode($request->getBody());
        $message = $this->t('Discourse user @username: @group divested.', [
          '@username' => $hostwire_user->user->username,
          '@group' => $group
        ]);
        \Drupal::logger('hostwire_sync')->notice($message);

        return $response;
      }
    }
    catch (\GuzzleHttp\Exception\GuzzleException $e) {
      watchdog_exception('hostwire_sync', $e, $e->getMessage());
    }
    return null;
  }

  /**
   * @param $name
   * @param string $method
   *
   * @return bool
   */
  protected function getGroupIdByName($name, $method = 'GET') {
    $url = $this->url . '/groups/search.json';

    try {
      $request = $this->client->request($method, $url, $this->getDefaultParameter());
      if ($request->getStatusCode() === 200) {
        $response = json_decode($request->getBody());

        foreach ($response as $group) {
          if ($group->name === $name) {
            return $group->id;
          }
        }
      }
    }
    catch (\GuzzleHttp\Exception\GuzzleException $e) {
      watchdog_exception('hostwire_sync', $e, $e->getMessage());
    }

    return FALSE;
  }

  /**
   * @param $user_entity
   *
   * @return bool|mixed
   */
  protected function getHostwireUser($user_entity) {
    $drupal_uid = $user_entity->get('uid')->value;
    return $this->getUserByExternalId($drupal_uid);
  }

  /**
   * @param $user_id
   * @param string $method
   *
   * @return bool|mixed
   */
  protected function getUserByExternalId($user_id, $method = 'GET') {
    $url = $this->url . '/users/by-external/' . $user_id . '.json';
    \Drupal::logger('hostwire_sync')->notice("Syncing user with ID: " . $user_id . ' with ' . $url);

    try {
      $request = $this->client->request($method, $url, $this->getDefaultParameter());
      if ($request->getStatusCode() === 200) {
        $response = json_decode($request->getBody());
        return $response;
      }
      else {
        return FALSE;
      }
    }
    catch (\GuzzleHttp\Exception\GuzzleException $e) {
      watchdog_exception('hostwire_sync', $e, $e->getMessage());
    }
    return FALSE;
  }

  /**
   * @param $user_entity
   * @param $groups
   *
   * @return bool|mixed
   */
  protected function createHostwireUser($user_entity, $groups) {
    $sso_secret = \Drupal::config('discourse_sso.settings')->get('discourse_sso_secret');
    $sso_params = array(
      'external_id' => $user_entity->get('uid')->value,
      'email' => $user_entity->get('mail')->value,
      'username' => $user_entity->get('field_user_hostwire_name')->value,
      'add_groups' => implode(",", $groups),
      'require_activation' => 'true',
    );
    $sso_payload = base64_encode( http_build_query( $sso_params ) );
    $sig = hash_hmac( 'sha256', $sso_payload, $sso_secret );
    $url = $this->url . '/admin/users/sync_sso';
    $post_fields = array(
      'sso' => $sso_payload,
      'sig' => $sig,
    );
    try {
      $request = $this->client->request('POST', $url, [
        'headers' => [
          'Accept-Encoding' => 'gzip',
          'Content-Type' => 'multipart/form-data',
          'Api-Key' => $this->api_key,
          'Api-Username' => $this->api_username,
        ],
        'form_params' => $post_fields,
      ]);
      
      if ($request->getStatusCode() === 200) {
        $response = json_decode($request->getBody());
        \Drupal::logger('hostwire_sync')->notice("Created new user in Hostwire: " . $user_entity->get('field_user_hostwire_name')->value);
        return $response;
      }
      else {
        return FALSE;
      }
    }
    catch (\GuzzleHttp\Exception\GuzzleException $e) {
      watchdog_exception('hostwire_sync', $e, $e->getMessage());
    }
    return FALSE;
  }

  /**
   * @param $user
   *
   * @return array
   */
  protected function getHostwireUserGroups($user) {
    $groups = [];
    foreach ($user->user->groups as $group) {
      if ($group->automatic === FALSE) {
        $groups[$group->id] = $group->name;
      }
    }
    return $groups;
  }
}
