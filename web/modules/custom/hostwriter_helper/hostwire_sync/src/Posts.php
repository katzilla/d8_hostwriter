<?php

namespace Drupal\hostwire_sync;

use Drupal\discourse_sso\SingleSignOnBase;
use Drupal\Component\Serialization\Json;


class Posts extends SingleSignOnBase {

  /**
   * Get latest posts from hostwire and return json response for frontend
   * @param string $method
   *
   * @return bool|mixed
   */
  public function getPosts($method = 'GET') {
    $url = $this->url . '/latest.json';
    \Drupal::logger('hostwire_sync')->notice("getting latest posts....");

    try {
      $request = $this->client->request($method, $url, [
        'headers' => [
          'Api-Key' => $this->api_key,
          'Api-Username' => $this->api_username,
        ]
      ]);
      if ($request->getStatusCode() === 200) {
        $posts = Json::decode((string) $request->getBody());
        return $posts ? $posts : FALSE;
      }
      else {
        return FALSE;
      }
    }
    catch (\GuzzleHttp\Exception\GuzzleException $e) {
      watchdog_exception('hostwire_sync', $e, $e->getMessage());
    }
    return FALSE;
  }

  /**
   * Function for getting categories to latest posts. Category data is included 
   * in the getPosts() response. 
   * 
   * @param string $method
   *
   * @return bool|mixed
   */
  public function getCategories($method = 'GET') {
    $url = $this->url . '/categories.json';
    \Drupal::logger('hostwire_sync')->notice("getting list of categories....");

    try {
      $request = $this->client->request($method, $url, [
        'headers' => [
          'Api-Key' => $this->api_key,
          'Api-Username' => $this->api_username,
        ]
      ]);
      if ($request->getStatusCode() === 200) {
        return Json::decode((string) $request->getBody());
      }
      else {
        return FALSE;
      }
    }
    catch (\GuzzleHttp\Exception\GuzzleException $e) {
      watchdog_exception('hostwire_sync', $e, $e->getMessage());
    }
    return FALSE;
  }
}
