<?php

namespace Drupal\hostwriter_helper\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure hostwriter_helper settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hostwriter_helper_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['hostwriter_helper.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('hostwriter_helper.settings');

    $form['cache_expire'] = [
      '#type' => 'number',
      '#title' => $this->t('Cache expiry time'),
      '#default_value' => $config->get('cache.expire'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('hostwriter_helper.settings');
    $config->set('cache.expire', $form_state->getValue('cache_expire'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
