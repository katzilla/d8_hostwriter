<?php

namespace Drupal\hostwriter_helper\Controller;

use function asort;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\TermStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\user\Entity\User;

/**
 * Create responses for hostwriter_helper JSON-Requests.
 */
class HostwriterController extends ControllerBase {

  /**
   * Journalists query string.
   *
   * @var string
   */
  protected $journalistsQuery = '
  select sum(journalists) journalists ,count(country) countries from
  (
    select count(users.uid) journalists, location.field_user_location_country_code country
    from users_field_data users
        inner   join user__field_user_location location on users.uid = location.entity_id and location.deleted = 0
        inner join user__roles roles
                   on roles.entity_id = users.uid and roles.roles_target_id = \'hostwriter\' and roles.deleted = 0

   where status = 1
   group by country
  ) count';

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Hostwriter config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Term storage.
   *
   * @var \Drupal\taxonomy\TermStorage
   */
  private $termStorage;

  /**
   * @inheritdoc
   */
  public function __construct(TermStorage $termStorage, ConfigFactory $config, CacheBackendInterface $cache) {
    $this->config = $config->get('hostwriter_helper.settings');
    $this->cache = $cache;
    $this->termStorage = $termStorage;
  }

  /**
   * @inheritdoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('taxonomy_term'),
      $container->get('config.factory'),
      $container->get('cache.default')
    );
  }

  /**
   * Queries the number of journalists a country from the database.
   */
  public function journalistCount() {
    $cid = \Drupal::routeMatch()->getRouteName();
    $result = $this->cache->get($cid);
    if (empty($result)) {
      $expire = $this->config->get('cache.expire');
      $db = \Drupal::database();
      $query = $db->query($this->journalistsQuery);
      if ($query->execute()) {
        $result = $query->fetch();
      }
      $this->cache->set($cid, $result, time() + $expire, ['hostwriter']);
    }
    else {
      $result = $result->data;
    }
    return new JsonResponse($result);
  }

  /**
   * JSON Response - Term list.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON-response;
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function taxonomies() {
    $language = \Drupal::languageManager()->getCurrentLanguage();
    $cid = sprintf("%s.%s",
      \Drupal::routeMatch()->getRouteName(),
      $language->getId());
    $cached = $this->cache->get($cid);
    if ($cached) {
      $result = $cached->data;
    }
    else {
      $cacheTags = $this->config->getCacheTags();
      $expire = $this->config->get('cache.expire');
      $taxonomies = $this->config->get('rest.queries.taxonomies');
      foreach ($taxonomies as $vid) {
        $tree = $this->termStorage->loadTree($vid, 0, 1, TRUE);
        $terms = [];
        /** @var \Drupal\taxonomy\Entity\Term $item */
        foreach ($tree as $item) {
          $translated = $item->getTranslation($language->getId());
          $terms[] = (object) [
            'name' => $translated->name->getString(),
            'value' => (int) $translated->tid->getString(),
            'target_id' => (int) $translated->tid->getString(),
          /*
               'url' => [
                          'canonical' => $translated->toUrl()->toString()
                        ]
           */
          ];
          $cacheTags = Cache::mergeTags($cacheTags, $translated->getCacheTags());
        }
        $result[$vid] = $terms;
      }
      $this->cache->set($cid, $result, time() + $expire, $cacheTags);
    }
    return new JsonResponse($result);
  }

  /**
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function user_filters() {
    // get media
    $media = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('media');
    foreach ($media as $term) {
      $data['media'][] = ['tid' => $term->tid, 'name' => $term->name];
    }
    // get Expertise
    $expertises = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('expertise');
    foreach ($expertises as $term) {
      $data['expertise'][] = ['tid' => $term->tid, 'name' => $term->name];
    }
    // get Organisations
    $organisations = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('organisations');
    foreach ($organisations as $term) {
      $data['organisation'][] = ['tid' => $term->tid, 'name' => $term->name];
    }
    // get the user field data
    $ids = \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('roles', 'Hostwriter')
      ->condition('uid', 1, '!=')
      ->execute();
    $users = User::loadMultiple($ids);
    $localities = [];
    foreach($users as $user){
      if (!empty($user->field_user_location[0]->locality)) {
        $localities[$user->field_user_location[0]->locality] = 1;
      }
      foreach ($user->field_user_languages as $language) {
        $field_array = $language->getValue();
        $languages[$field_array['language']] = 0;
      }
    }
    ksort($localities);
    foreach (array_keys($localities) as $locality) {
      $data['locality'][] = ['name' => $locality];
    }
    foreach (array_keys($languages) as $language) {
      $data['language'][] = ['name' => $language];
    }
    $countries = \Drupal::service('address.country_repository')->getList();
    foreach ($countries as $country_code => $country) {
      $data['country'][] = ['country_code' => $country_code, 'name' => $country];
    }
    return new JsonResponse($data);
  }
}
