<?php

namespace Drupal\user_languages\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'languages' field type.
 *
 * @FieldType(
 *   id = "user_languages",
 *   label = @Translation("User Languages"),
 *   description = @Translation("Fieldtype for adding spoken languages and level of speaking"),
 *   default_widget = "user_languages_widget",
 *   default_formatter = "user_languages_formatter"
 * )
 */
class UserLanguagesItem extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
        'language' => 'Arabic',  
        'level' => 'Fluent',
      ] + parent::defaultStorageSettings();
  }
  
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [];
    $columns['language'] = [
      'type' => 'varchar',
      'length' => 255,
    ];
    $columns['level'] = [
      'type' => 'varchar',
      'length' => 255,
    ];

    return [
      'columns' => $columns,
      'indexes' => ['language' => ['language']],
    ];
  }

  
  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties = [];
    
    $properties['language'] = DataDefinition::create('string')
      ->setLabel(t('Language'));

    $properties['level'] = DataDefinition::create('string')
      ->setLabel(t('Level'));

    return $properties;
  }


  /**
   * {@inheritdoc}
   */
  public function isEmpty() {

    $isEmpty =
      empty($this->get('language')->getValue()) &&
      empty($this->get('level')->getValue());

    return $isEmpty;
    
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    

    $element['language'] = [
      '#type' => 'textarea',
      '#title' => t('Allowed languages list'),
      '#default_value' => $this->getSetting('language'),
      '#rows' => 10,
      '#field_has_data' => $has_data,
      '#field_name' => $this->getFieldDefinition()->getName(),
      '#entity_type' => $this->getEntity()->getEntityTypeId(),
      '#allowed_values' => $this->getSetting('language'),
    ];
    
    $element += parent::storageSettingsForm($form, $form_state, $has_data);
    return $element;
  }

  /**
   * Generates a string representation of an array of 'allowed values'.
   *
   * This string format is suitable for edition in a textarea.
   *
   * @param array $values
   *   An array of values, where array keys are values and array values are
   *   labels.
   *
   * @return string
   *   The string representation of the $values array:
   *    - Values are separated by a carriage return.
   *    - Each value is in the format "value|label" or "value".
   */
  protected function allowedValuesString($values) {
    $lines = [];
    foreach ($values as $key => $value) {
      $lines[] = "$key|$value";
    }
    return implode("\n", $lines);
  }

}
