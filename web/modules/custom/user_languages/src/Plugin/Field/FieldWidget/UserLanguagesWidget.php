<?php

namespace Drupal\user_languages\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'languages' widget.
 *
 * @FieldWidget(
 *   id = "user_languages_widget",
 *   label = @Translation("Languages"),
 *   field_types = {
 *     "user_languages"
 *   }
 * )
 */
class UserLanguagesWidget extends WidgetBase {
  
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'level' => 'Fluent',
        'placeholder' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $setting_language = $this->getFieldSetting('language');
    $options_language = array();
    $lines = explode(PHP_EOL, $setting_language);
    foreach($lines as $line) {
      $line = explode('|', $line);
      $options_language[$line[0]] = $line[1];
    }
    
    $element['language'] = [
      '#type' => 'select',
      '#title' => t('Language'),
      '#options' => $options_language,
      '#default_value' => isset($items[$delta]->language) ? $items[$delta]->language : null,
      '#empty_option' => 'please choose language',
      '#placeholder' => t('Language'),
    ];

    $element['level'] = [
      '#type' => 'select',
      '#title' => t('Level'),
      '#options' => ['Fluent', 'Intermediate', 'Beginner'],
      '#default_value' => isset($items[$delta]->level) ? $items[$delta]->level : null,
      '#empty_option' => 'please choose level of speaking',
      '#placeholder' => t('Level'),
    ];

  
    return $element;
  }


  /**
   * {@inheritdoc}
   */
/*  public static function defaultSettings() {
    return [
        'language' => 60,
        'placeholder' => '',
      ] + parent::defaultSettings();
  }*/

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['language'] = [
      '#type' => 'text',
      '#title' => $this->t('Available languages'),
      '#default_value' => $this->getSetting('language'),
      '#required' => TRUE,
      '#size' => 10,
      '#min' => 1,
    ];
    $elements['level'] = [
      '#type' => 'text',
      '#title' => $this->t('Available level'),
      '#default_value' => $this->getSetting('level'),
      '#required' => TRUE,
      '#size' => 10,
      '#min' => 1,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t('Textfield size: @size', ['@size' => $this->getSetting('level')]);
    if (!empty($this->getSetting('placeholder'))) {
      $summary[] = t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('language')]);
    }

    return $summary;
  }

}
