<?php

namespace Drupal\user_ratings\Plugin\Field\FieldType;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the 'ratings' field type.
 *
 * @FieldType(
 *   id = "user_ratings",
 *   label = @Translation("User Ratings"),
 *   description = @Translation("Fieldtype for adding recommendations to users"),
 *   default_widget = "user_ratings_widget",
 *   default_formatter = "entity_reference_label",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList"
 * )
 */
class UserRatingsItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
   $properties = parent::propertyDefinitions($field_definition);
   $properties['rating_text'] = DataDefinition::create('string')
     ->setLabel(new TranslatableMarkup('Rating text'));
   $properties['rating_format'] = DataDefinition::create('filter_format')
     ->setLabel(t('Rating text format'));
    return $properties;
  }

  
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['rating_text'] = [
      'type' => 'varchar',
      'length' => 255,
    ];
    $schema['columns']['rating_format'] = [
      'type' => 'varchar',
      'length' => 255,
    ];
    return $schema;
  }


  /**
   * {@inheritdoc}
   */
  public function applyDefaultValue($notify = TRUE) {
    $this->setValue(['rating_format' => 'basic_html'], $notify);
    return $this;
  }

  
  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $isEmpty = empty($this->get('rating_text')->getValue());
    return $isEmpty;

  }
}
