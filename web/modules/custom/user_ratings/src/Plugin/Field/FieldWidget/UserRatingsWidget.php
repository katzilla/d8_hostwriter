<?php

namespace Drupal\user_ratings\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'ratings' widget.
 *
 * @FieldWidget(
 *   id = "user_ratings_widget",
 *   label = @Translation("Ratings"),
 *   description = @Translation("An autocomplete user-reference field with rating text"),
 *   field_types = {
 *     "user_ratings"
 *   }
 * )
 */
class UserRatingsWidget extends EntityReferenceAutocompleteWidget {

  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $widget = parent::formElement($items, $delta, $element, $form, $form_state);
    $widget['rating_text'] = array(
      '#title' => $this->t('Rating text'),
      '#type' => 'text_format',
      '#format' => 'basic_html',
      '#default_value' => isset($items[$delta]) ? $items[$delta]->rating_text : '',
      '#weight' => 10,
      '#empty_value' => '',
    );
    return $widget;
  }
  
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach($values as $key => $value) {
      $values[$key]['rating_text'] = $value['rating_text']['value']; // json_encode($value['rating_text']['value'])
      $values[$key]['rating_format'] = 'basic_html';
    }
    return $values;
  }
}
