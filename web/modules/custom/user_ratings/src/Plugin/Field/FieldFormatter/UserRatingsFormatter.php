<?php

namespace Drupal\user_ratings\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;

/**
 * Plugin implementation of the 'ratings' formatter.
 *
 * @FieldFormatter(
 *   id = "user_ratings_formatter",
 *   label = @Translation("Ratings"),
 *   description = @Translation("Display the referenced entities label with their texts."),
 *   field_types = {
 *     "user_ratings"
 *   }
 * )
 */
class UserRatingsFormatter extends EntityReferenceLabelFormatter {

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'processed_text',
        '#text' => $item->rating_text,
        '#format' => $item->rating_format,
      ];
    }
    return $elements;
  }
}
