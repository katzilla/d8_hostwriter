# Adjustments for hostwriter

## php-Version

Fixed in composer.json to ^7.1.0.

## Environment

Define the actual environment inside the .env file in root-directory:

```
ENVIRONMENT=development|testing|production

```

Create this file on each machine and define the appropriate environment
This file shall not be checked into the repository!

## support modules

### config_split
Enables the sitebuilder to split configuration depending on the
environment.

Split configurations are stored in '/config/ENVIRONMENT'

#### split configuration

|module|development|testing|production|
|----|----|----|----|
| stage_file_proxy | X | (X) | -- |
| devel | X | -- | -- |

(X) switch on as soon as production server is running


### simplei
Simple Environment Indicator - changes the color of the admin toolbar
to indicate which environment is used.

<span style="background-color:FireBrick">Production: FireBrick</span>
<span style="background-color:GoldenRod">Testing/Staging: GoldenRod</span>
<span style="background-color:DodgerBlue">Development: DodgerBlue</span>

### stage_file_proxy

Copies images from the live-server to local server, this eliminates the
need to copy the files folder manually to the local machine.

## Drupal scaffold

Only absolute necessary scaffold files are loaded by the composer:

- index.php
- robots.txt
- .htaccess

These files are not checked into the repository, when these files have to
be changed, add them to git and remove them from composer install.

## Settings

### trusted_host_patterns

- set in settings.php to live server
- adjust inside settings.local.php on development and testing environments

